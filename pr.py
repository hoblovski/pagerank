print("reading node map")
with open("node.map.utf8", "r") as fin:
    maps = {}
    for l in fin:
        v1, v2 = l.split("-->")
        maps[int(v2)] = v1

print("rewriting lastresult")
with open("build/lastresult", "r") as fin,\
        open("lastresulth", "w") as fout:
    for l in fin:
        v1, v2 = l.split(": ")
        print("%s: %s" % (maps[int(v1)], v2), file=fout)

